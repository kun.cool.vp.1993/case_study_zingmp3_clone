export {default as instance} from './axios';
export * from './home';
export * from './music';
export * from './album';