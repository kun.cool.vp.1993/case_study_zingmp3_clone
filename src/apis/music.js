/* eslint-disable no-async-promise-executor */
import api from "./axios"

// eslint-disable-next-line no-async-promise-executor
export const apiGetSong = (songId) => new Promise(async (resolve, reject) => {
    try {
        const response = await api({
            url: 'song',
            method: 'get',
            params: {id: songId},
        })
        resolve(response)
    } catch (error) {
        reject(error)
    }
})
// eslint-disable-next-line no-async-promise-executor
export const apiGetDetailSong = (songId) => new Promise(async (resolve, reject) => {
    try {
        const response = await api({
            url: 'infosong',
            method: 'get',
            params: {id: songId},
        })
        resolve(response)
    } catch (error) {
        reject(error)
    }
})


export const apiSearch = (keyword) => new Promise(async (resolve, reject) => {
    try {
        const response = await api({
            url: 'search',
            method: 'get',
            params: {keyword},
        })
        resolve(response)
    } catch (error) {
        reject(error)
    }
})




// // eslint-disable-next-line no-async-promise-executor
// export const apiGetDetailPlaylist = (playlistId) => new Promise(async (resolve, reject) => {
//     try {
//         const response = await api({
//             url: 'detailplaylist',
//             method: 'get',
//             params: {id: playlistId},
//         })
//         resolve(response)
//     } catch (error) {
//         reject(error)
//     }
// })