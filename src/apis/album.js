// eslint-disable-next-line no-async-promise-executor
import api from "./axios"


// eslint-disable-next-line no-async-promise-executor
export const apiGetDetailPlaylist = (playlistId) => new Promise(async (resolve, reject) => {
    try {
        const response = await api({
            url: 'detailplaylist',
            method: 'get',
            params: {id: playlistId},
        })
        resolve(response)
    } catch (error) {
        reject(error)
    }
})