import api from "./axios"

// eslint-disable-next-line no-async-promise-executor
export const getHome = () => new Promise(async (resolve, reject) => {
    try {
        const response = await api({
            url: 'home',
            method: 'get'
        })
        resolve(response)
    } catch (error) {
        reject(error)
    }
})