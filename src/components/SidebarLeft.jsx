import logo from "../assets/favicon.png";
import { NavLink, useNavigate } from "react-router-dom";
import { path, sidebarMenuItems } from "../utilities";
const notActiveStyle =
  "py-2 px-[25px] font-bold text-[#32323d] text-[13px] flex gap-x-[12px] items-center";
const activeStyle =
  "py-2 px-[25px] font-bold text-[#0f7070] text-[13px] flex gap-x-[12px] items-center";
function SidebarLeft() {
  const navigate = useNavigate();
  return (
    <div className="flex flex-col h-full bg-[#dde4e4]">
      <div
      onClick={()=>navigate(path.HOME)} 
      className="w-full h-[70px] py-[15px] px-[25px] flex justify-center items-center cursor-pointer">
        <img src={logo} alt="logo" className="w-[50px] h-[50px]" />
      </div>
      <div className={"flex flex-col"}>
        {sidebarMenuItems.map((item, index) => (
          <NavLink
            key={index}
            to={item.path}
            end={item.end}
            className={({ isActive }) =>
              isActive ? activeStyle : notActiveStyle
            }
          >
            {item.icon}
            <span>{item.text}</span>
          </NavLink>
        ))}
      </div>
    </div>
  );
}

export default SidebarLeft;
