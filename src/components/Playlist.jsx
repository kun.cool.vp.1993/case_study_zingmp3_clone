import PropTypes from "prop-types";
import { memo } from "react";
import { useNavigate } from "react-router-dom";
import { icons } from "../utilities";
import { getDisplayStr } from "../utilities/helper";
import { Image } from "./Image";
const { FaChevronRight } = icons;

// eslint-disable-next-line react-refresh/only-export-components
function Playlist({ playlist, hideSpan ,nonPadding, children }) {
  // console.log(playlist);
  const { title, items, link } = playlist;
  console.log(items);

  const navigate = useNavigate();
  const handleNavigate = (link) => {
    navigate(link);
  };
  // if (title === "Album Hot") console.log(playlist);
  return (
    <div className={`mt-12 px-[${nonPadding? '0': '59'}px] flex flex-col gap-5`}>
      <div className="flex justify-between">
        {children ? children :<span className="text-[20px] font-bold">{title}</span>}
        {!hideSpan && (
          <span
            onClick={() => handleNavigate(link.split(".")[0])}
            className="flex items-center justify-center gap-1 cursor-pointer text-gray-700 hover:text-green-800"
          >
            <span className="text-xs ">TẤT CẢ</span>
            <span className="flex items-center justify-center">
              <FaChevronRight size={12} />
            </span>
          </span>
        )}
      </div>
      <div className="flex gap-4 justify-between">
        {items?.slice(0, 5)?.map((item) => (
          <div
            key={item?.encodeId}
            onClick={() => handleNavigate(item?.link?.split(".")[0])}
            className="flex flex-col gap-1"
          >
            <div className="cursor-pointer">
              <Image src={item?.thumbnailM } />
            </div>
            <div className="flex items-center text-sm font-semibold">
              {getDisplayStr(
                title === "Album Hot" ? item?.title : item?.sortDescription,
                title === "Top 100" || title === "Album Hot" ? 29 : 40
              )}
            </div>
            <div className="flex items-center text-xs">
              {title === "Top 100"
                ? item?.artists?.map((artist) => artist.name).join(", ")
                : title === "Album Hot"
                ? item?.artistsNames
                : ""}
            </div>
          </div>
        ))}
      </div>
    </div>
  );
}
Playlist.propTypes = {
  playlist: PropTypes.object.isRequired,
  hideSpan: PropTypes.bool,
  nonPadding: PropTypes.bool,
  children: PropTypes.node
};
// eslint-disable-next-line react-refresh/only-export-components
export default memo(Playlist);
