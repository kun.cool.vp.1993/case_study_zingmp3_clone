import { useDispatch } from "react-redux";
import * as actions from "../../store/actions";
import { checkReleaseDate, getDisplayStr } from "../../utilities/helper";
import PropTypes from "prop-types";

function SongItem({ item, order, percent, small, showTime, height, padding, size , bgColor}) {
  const dispatch = useDispatch();
  return (
    <>
      {item && (
        <div
          onClick={() => {
            dispatch(actions.setCurrSongId(item?.encodeId));
            dispatch(
              actions.addToRecent({
                thumbnail: item?.thumbnail,
                title: item?.title,
                artistsNames: item?.artistsNames,
                encodeId: item?.encodeId,
              })
            );
          }}
          className={`h-[${height ? height : "80"}px] p-[${
            padding ? padding : "10"
          }px] flex justify-between gap-2 items-center rounded-md  hover:text-white ${
            bgColor? bgColor :
            order
              ? "text-white bg-[hsla(0,0%,100%,.07)] hover:bg-[#954ea7]"
              : "text-black hover:bg-main-500"
          }`}
        >
          <div className="flex gap-3 items-center w-full">
            <div className="flex items-center justify-center">
              {order && (
                <span
                  className={`text-shadow-display-${order} hover:bg-[#954ea7] text-3xl w-5`}
                >
                  {order}
                </span>
              )}
            </div>
            <div className="cursor-pointer">
              <img
                src={item?.thumbnail}
                alt="thumbnail"
                className={` ${size ? size :
                  small ? "h-[40px] w-[40px]" : "h-[60px] w-auto"
                } rounded-md`}
              />
            </div>
            <div className="flex flex-col">
              <span className="text-[14px] font-semibold">
                {getDisplayStr(item.title, order ? 38 : 28)}
              </span>
              <span className="text-xs cursor-pointer hover:underline">
                {item.artistsNames}
              </span>
              {!order && showTime && (
                <span className="text-xs">
                  {checkReleaseDate(item.releaseDate)}
                </span>
              )}
            </div>
          </div>
          {percent && <span className="font-extrabold">{percent}%</span>}
        </div>
      )}
    </>
  );
}

SongItem.propTypes = {
  item: PropTypes.object.isRequired,
  order: PropTypes.number,
  percent: PropTypes.number,
  small: PropTypes.bool,
  showTime: PropTypes.bool,
  height: PropTypes.number,
  padding: PropTypes.number,
  size: PropTypes.string,
  bgColor: PropTypes.string,
};

export default SongItem;
