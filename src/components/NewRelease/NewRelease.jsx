import { memo, useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { icons } from "../../utilities";
import SongItem from "./SongItem";

const { FaChevronRight } = icons;
const notActiveStyle =
  "hover:text-gray-500 hover:border-gray-500 border-gray-700 text-gray-700";
const activeStyle =
  "text-gray-50 bg-main-500 border-main-500 hover:bg-main-600 hover:border-blue-600 hover:text-white ";

const menuItems = [
  {
    label: "Tất cả",
    key: "all",
    isActive: true,
  },
  {
    label: "Việt Nam",
    key: "vPop",
    isActive: false,
  },
  {
    label: "Quốc Tế",
    key: "others",
    isActive: false,
  },
];
const deActiveItems = () =>
  menuItems.forEach((menuItem) => (menuItem.isActive = false));

// eslint-disable-next-line react-refresh/only-export-components
function NewRelease() {
  const data = useSelector((state) => state.app.newRelease);

  const [items, setItems] = useState([]);
  useEffect(() => {
    setItems(data?.items?.all.slice(0, 12));

    return () => {};
  }, [data]);

  const navigate = useNavigate();
  const handleNavigate = () => {
    navigate(data?.link);
  };
  return (
    <div className="mt-12 px-[59px] flex flex-col gap-5">
      <div>
        <h3 className="text-[20px] font-bold">{data?.title}</h3>
      </div>
      <div className="flex items-center justify-between">
        <div className="flex gap-4">
          {menuItems.map((item) => (
            <button
              key={item.key}
              className={`px-7 border h-6 rounded-l-full rounded-r-full hover:text-gray-500 ${
                item.isActive ? activeStyle : notActiveStyle
              }`}
              onClick={() => {
                setItems(data?.items[item.key]?.slice(0, 12));
                deActiveItems();
                item.isActive = true;
              }}
            >
              {item.label}
            </button>
          ))}
        </div>
        <span
          onClick={handleNavigate}
          className="flex items-center justify-center gap-1 cursor-pointer text-gray-700 hover:text-green-800"
        >
          <span className="text-xs ">TẤT CẢ</span>
          <span className="flex items-center justify-center">
            <FaChevronRight size={12} />
          </span>
        </span>
      </div>
      <div className="grid grid-cols-2 min-[1024px]:grid-cols-3 gap-x-4">
        {items?.map((item) => (
          <SongItem key={item.encodeId} item={item} />
        ))}
      </div>
    </div>
  );
}

// eslint-disable-next-line react-refresh/only-export-components
export default memo(NewRelease);
