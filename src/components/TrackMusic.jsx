import PropTypes from "prop-types";
import { icons } from "../utilities";
import { memo } from "react";
import { getDisplayStr, convertSecondsToMins } from "../utilities/helper";
import { useDispatch, useSelector } from "react-redux";
import * as actions from "../store/actions";

const { CiMusicNote1 } = icons;
//eslint-disable-next-line react-refresh/only-export-components
function TrackMusic({ songData, playlist, hideAlbum }) {
  const dispatch = useDispatch();
  const album = useSelector((st) => st.album?.album);
  return (
    <div
      className="flex 
      justify-between 
      items-center 
      p-[10px] 
      border-t 
      border-[rgba(0,0,0,0.05)] 
      hover:bg-[#dde4e4] 
      cursor-pointer"
      onClick={() => {
        dispatch(actions.setCurrSongId(songData?.encodeId));
        dispatch(actions.atAlbum(playlist.map((item) => item.encodeId)));
        dispatch(actions.setCurrPlaylist(album));
        dispatch(
          actions.addToRecent({
            thumbnail: songData?.thumbnail,
            title: songData?.title,
            artistsNames: songData?.artistsNames,
            encodeId: songData?.encodeId,
          })
        );
      }}
    >
      <div className="flex items-center gap-3 flex-10">
        {!hideAlbum && <span>
          <CiMusicNote1 title={songData?.encodeId} />
        </span>}
        <div className="relative">
          <img
            src={songData?.thumbnail}
            alt="thumbnail"
            className="rounded-lg w-10 h-10 object-cover"
          />
        </div>
        <span className="flex flex-col">
          <span className="text-sm font-semibold">
            {getDisplayStr(songData?.title, 40)}
          </span>
          <span className="text-xs opacity-70">{getDisplayStr(songData?.artistsNames, 20)} </span>
        </span>
      </div>
      {!hideAlbum && <div className="flex-8 flex justify-start">
        {songData?.album
          ? getDisplayStr(songData?.album?.title, 40)
          : "Unknown"}
      </div>}
      <div className="flex-2 flex justify-end text-xs opacity-70">
        {convertSecondsToMins(songData?.duration).replace("00:", "")}
      </div>
    </div>
  );
}

TrackMusic.propTypes = {
  songData: PropTypes.object.isRequired,
  playlist: PropTypes.array,
  hideAlbum: PropTypes.bool
};
//eslint-disable-next-line react-refresh/only-export-components
export default memo(TrackMusic);
