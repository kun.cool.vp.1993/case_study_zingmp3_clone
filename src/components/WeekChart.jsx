import { useSelector } from "react-redux";
import { Link } from "react-router-dom";

function WeekChart() {
  const { weekChart: {items} } = useSelector((st) => st.app);
  return (
    <div className="flex items-center px-[43px] w-full mt-12">
      {items && items?.map((item) => (
        <Link
          key={item?.link}
          to={item?.link?.split(".")[0]}
          className="flex-1 px-4"
        >
          <img
            src={item?.cover}
            alt="cover"
            className="w-full object-cover rounded-md"
          />
        </Link>
      ))}
    </div>
  );
}

export default WeekChart;
