
import PropTypes from 'prop-types'
import { handleNumber } from '../utilities'
import {icons} from '../utilities'
import { Image } from './Image'
import { useNavigate } from 'react-router-dom'
const {AiOutlineUserAdd} = icons


function Artist({image, title, followers, link}) {
    const navigate = useNavigate();

  return (
    <div className='flex-1 flex flex-col gap-[15px]'>
        <Image src={image} alt="single" isRoundedFull hideIcon className='w-full object-contain rounded-full' link = {link}/>
        <div className='flex gap-1 flex-col items-center'>
            <span onClick={()=> navigate(link)} className='text-sm font-medium cursor-pointer hover:underline'>{title}</span>
            <span className='text-xs opacity-70'>
                {`${handleNumber(followers)} quan tâm`}
            </span>
            <button type='button'
            className='bg-main-500 px-4 py-1 text-white text-sm rounded-l-full rounded-r-full flex items-center justify-center gap-1'>
                <span>
                    <AiOutlineUserAdd />
                </span>
                <span className='text-xs opacity-70'>
                    QUAN TÂM
                </span>
            </button>
        </div>
    </div>
  )
}

Artist.propTypes = {
    image: PropTypes.string,
    title: PropTypes.string,
    followers: PropTypes.number,
    link: PropTypes.string
  };

export default Artist