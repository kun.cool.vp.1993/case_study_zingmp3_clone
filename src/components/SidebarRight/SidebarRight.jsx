import { useEffect, useState } from "react";
import { getDisplayStr, icons } from "../../utilities";
import { useSelector } from "react-redux";
import { SidebarItem } from "./";
import Scrollbars from "react-custom-scrollbars-2";

const { ImBin, IoMdAlarm } = icons;

function SidebarRight() {
  const [isRecent, setIsRecent] = useState(false);
  const { songInfo, isPlaying } = useSelector((st) => st.music);
  const showPlaylist = useSelector((st) => st?.music?.playlist?.song?.items);
  const recentList = useSelector((st) => st.music.recentList);
  
  useEffect(()=> {
    if(isPlaying){ 
        setIsRecent(false);
    }
  },[isPlaying])
  return (
    <div className="flex flex-col text-xs w-full h-display bg-main-200 border border-l-gray-400 ">
      <div className="h-[70px] w-full flex-none flex px-2 items-center justify-evenly gap-2 text-gray-700">
        <div className="flex bg-main-100 rounded-l-full rounded-r-full">
          <span
            onClick={() => {
              setIsRecent(false);
            }}
            className={`px-4 py-2  ${
              !isRecent
                ? "bg-gray-400 hover:text-gray-200"
                : "bg-main-100 hover:text-black "
            }  rounded-l-full rounded-r-full cursor-pointer`}
          >
            Danh sách phát
          </span>
          <span
            onClick={() => {
              setIsRecent(true);
            }}
            className={`px-4 py-2  ${
              isRecent
                ? "bg-gray-400 hover:text-gray-200"
                : "bg-main-100 hover:text-black"
            }   rounded-l-full rounded-r-full cursor-pointer`}
          >
            Nghe gần đây
          </span>
        </div>
        <span className="p-2 rounded-full hover:ng-[#ced9d9] border bg-gray-400 cursor-pointer hover:text-black hover:bg-main-200">
          <IoMdAlarm size={14} />
        </span>
        <span className="p-2 rounded-full hover:ng-[#ced9d9] border bg-gray-400 cursor-pointer hover:text-black hover:bg-main-200">
          <ImBin size={14} />
        </span>
      </div>
      {!isRecent
        ? showPlaylist &&
          songInfo && (
            <div className="w-full flex-auto flex flex-col p-2">
              <Scrollbars autoHide style={{ width: "100%", height: "100%" }}>
                <SidebarItem item={songInfo} isListening />
                <div className="flex flex-col text-black pt-[15px] px-2 pb-[15px]">
                  <span className="text-sm font-bold">Tiếp theo</span>
                  <span className="opacity-70 text-xs flex gap-1 py-2">
                    <span>Từ Playlist </span>
                    <span className="font-semibold text-main-500">
                      {getDisplayStr(songInfo?.album?.title || "", 34)}
                    </span>
                  </span>
                  {showPlaylist &&
                    showPlaylist?.map((item) => (
                      <SidebarItem item={item} key={item.encodeId} />
                    ))}
                </div>
              </Scrollbars>
            </div>
          )
        : recentList && (
            <div className="w-full flex-auto flex flex-col p-2">
              <Scrollbars autoHide style={{ width: "100%", height: "100%" }}>
                {/* <SidebarItem item={recentList[0]} isListening /> */}
                {recentList &&
                  recentList?.map((item) => (
                    <SidebarItem item={item} key={item.encodeId} />
                  ))}
              </Scrollbars>
            </div>
          )}
    </div>
  );
}

export default SidebarRight;
