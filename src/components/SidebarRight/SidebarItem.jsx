import { useDispatch } from "react-redux";
import * as actions from "../../store/actions";
import { getDisplayStr } from "../../utilities/helper";
import PropTypes from "prop-types";

const active = 'flex bg-main-500 justify-start gap-2 items-center rounded-md text-white py-2 h-[56px]'
const normal = 'flex bg-main-200 justify-start gap-2 items-center rounded-md text-black py-2 h-[56px] hover:bg-main-400 hover:text-main-500'
function SidebarItem({ item , isListening}) {
  const dispatch = useDispatch();
  return (
    <>
      {item && (
        <div
          onClick={() => dispatch(actions.setCurrSongId(item?.encodeId))}
          className={`${isListening?active:normal}`}
        >
          <div className="flex gap-3 items-center justify-start">
            <div className="flex items-center justify-center">
            </div>
            <div className="cursor-pointer">
              <img
                src={item?.thumbnail}
                alt="thumbnail"
                className={`h-[40px] w-[40px] rounded-md`}
              />
            </div>
            <div className="flex flex-col">
              <span className="text-[14px] font-semibold">
                {getDisplayStr(item?.title,  28)}
              </span>
              <span className="text-xs cursor-pointer hover:underline">
                {(item.artistsNames)}
              </span>
            </div>
          </div>
        </div>
      )}
    </>
  );
}

SidebarItem.propTypes = {
  item: PropTypes.object.isRequired,
  isListening: PropTypes.bool
};

export default SidebarItem;