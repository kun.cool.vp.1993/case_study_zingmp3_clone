import { memo } from "react";
import { useSelector } from "react-redux";
import {getDisplayStr} from '../utilities'
import { useNavigate } from "react-router-dom";

// eslint-disable-next-line react-refresh/only-export-components
function Section() {
  const { friday } = useSelector((st) => st.app);
  const navigate = useNavigate();

  return (
    <div className="mt-12 px-[59px] flex flex-col gap-5">
      <div className="flex items-center justify-between">
        <h3 className="text-[20px] font-bold">{friday?.title}</h3>
        <span className="text-xs">TẤT CẢ</span>
      </div>
      <div className="flex items-center justify-between gap-[28px]">
        {friday &&
          friday?.items?.length > 0 &&
          friday?.items.map((item) => (
            <div 
            onClick={()=>{ 
              navigate(item?.link?.split('.')[0])
             }}
            key={item?.encodeId} 
            className="flex flex-col gap-2 flex-auto w-1/5 text-sm">
              <img src={item?.thumbnailM} alt="avatar" 
              className="w-full gap-3 h-auto rounded-lg cursor-pointer"/>
              <span className="flex flex-col">
                <span className="font-semibold">{item?.title}</span>
                <span>{getDisplayStr(item?.sortDescription,40)}</span>
              </span>
            </div>
          ))}
      </div>
    </div>
  );
}

// eslint-disable-next-line react-refresh/only-export-components
export default memo(Section);
