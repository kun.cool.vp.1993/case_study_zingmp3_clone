import { useNavigate } from "react-router-dom";
import { icons, path } from "../utilities";
import Search from "./Search";

const {FiLogIn} = icons

const { HiArrowNarrowLeft, HiArrowNarrowRight } = icons;

function Header() {
  const navigate = useNavigate()
  const handleNavigate = () =>{ 
    navigate(path.LOGIN)
  }
  return (
    <div className=" flex justify-between w-full items-center">
      <div className="flex gap-6 w-full">
        <div className="flex gap-6 text-gray-400 items-center">
          <span className="cursor-pointer">
            <HiArrowNarrowLeft size={24} />
          </span>
          <span className="cursor-pointer">
            <HiArrowNarrowRight size={24} />
          </span>
        </div>
        <div className="w-2/3">
            <Search />
        </div>
      </div>
      <div>
        <span
        onClick={handleNavigate} 
        className="h-[40px] flex items-center justify-evenly gap-1 w-[100px] rounded-l-full rounded-r-full bg-main-500 text-gray-200 cursor-pointer hover:bg-gray-200 hover:text-main-500">
          <span className="pl-2"><FiLogIn size={14}/></span>
          <span className="pr-2">LOG IN</span>
        </span>
      </div>
    </div>
  );
}

export default Header;
