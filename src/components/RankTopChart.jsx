import { memo, useEffect, useRef, useState } from "react";
import bgChart from "../assets/bg-chart.jpeg";
import { Line } from "react-chartjs-2";
// eslint-disable-next-line no-unused-vars
import { Chart } from "chart.js/auto";
import { useSelector } from "react-redux";
import SongItem from "./NewRelease/SongItem";
import { isEqual } from "lodash";
import { Link, useNavigate } from "react-router-dom";
import { path } from "../utilities";
import { icons } from "../utilities";

const { BsFillPlayFill } = icons;

// eslint-disable-next-line react-refresh/only-export-components
function RankTopChart() {
  const [data, setData] = useState(null);
  const RTChart = useSelector((st) => st.app.RTChart);
  const { chart, items } = RTChart;
  const chartRef = useRef(null);
  const navigate = useNavigate();
  const [selected, setSelected] = useState(null);
  const [tooltip, setTooltip] = useState({
    opacity: 0,
    top: 0,
    left: 0,
    date: "",
    value: "",
  });
  const options = {
    responsive: true,
    pointRadius: 0,
    maintainAspectRatio: false,
    scales: {
      y: {
        ticks: { display: false },
        grid: { color: "rgba(255,255,255,0.1)", drawTicks: false },
        min: chart?.minScore,
        max: chart?.maxScore,
        border: { dash: [3, 4] },
      },
      x: {
        ticks: { color: "white" },
        grid: { color: "transparent" },
      },
    },
    plugins: {
      legend: false,
      tooltip: {
        enabled: false,
        external: (context) => {
          const tooltipModel = context.tooltip;
          // if(!chart || !chart.current) return
          if (tooltipModel.opacity === 0) {
            if (tooltip.opacity !== 0)
              setTooltip((prev) => ({
                ...prev,
                opacity: 0,
              }));
            return;
          }
          const counters = [];
          for (let i = 0; i < 3; i++) {
            counters.push({
              data: chart?.items[Object.keys(chart?.items)[i]]
                ?.filter(
                  (i) =>
                    +i.hour ===
                    +tooltipModel.dataPoints[0].label.replace(":00", "")
                )
                ?.map((i) => i.counter),
              encodeId: Object.keys(chart?.items)[i],
            });
          }
          const result = counters.find((i) =>
            i.data.some(
              (score) =>
                score === +tooltipModel.body[0].lines[0].replace(",", "")
            )
          );
          const newTooltipData = {
            opacity: 1,
            left: tooltipModel.caretX + 10,
            top: tooltipModel.caretY - 80,
            date: +tooltipModel.dataPoints[0].label.replace(":00", ""),
            value: tooltipModel.dataPoints[0].formattedValue,
          };
          if (!isEqual(tooltip, newTooltipData)) setTooltip(newTooltipData);
          setSelected(result.encodeId);
        },
      },
    },
    hover: {
      mode: "dataset",
      intersect: false,
    },
  };
  useEffect(() => {
    const labels = chart?.times
      ?.filter((i) => +i.hour % 2 === 0)
      ?.map((i) => `${i.hour}:00`);
    const datasets = [];
    for (let i = 0; i < 3; i++) {
      datasets.push({
        data: chart?.items[Object.keys(chart?.items)[i]]
          ?.filter((i) => +i.hour % 2 === 0)
          ?.map((i) => i.counter),
        borderColor: i === 0 ? "#4a90e2" : i === 1 ? "#50e3c2" : "#e35050",
        tension: 0.2,
        borderWidth: 2,
        pointBorderColor: i === 0 ? "#4a90e2" : i === 1 ? "#50e3c2" : "#e35050",
        pointHoverRadius: 6,
        pointBackgroundColor: "white",
        pointHoverBorderWidth: 3,
      });
    }
    setData({ labels, datasets });
  }, [chart]);
  // const index = items.map((item, index) => {
  //   if (item.encodeId === selected) console.log(index);
  //   return index;
  // });
  return (
    <div className="relative mx-[59px] mt-12 max-h-[400px] rounded-lg">
      <img
        src={bgChart}
        alt="Top Rank Chart"
        className="w-full rounded-lg max-h-[400px]"
      />
      <div className="absolute rounded-lg z-10 top-0 left-0 right-0 bottom-0 bg-gradient-to-t from-[rgba(85,10,104,1)] to-[rgba(115,20,140,0.7)]"></div>
      <div className="absolute z-20 top-0 left-0 right-0 bottom-0 rounded-lg p-5 flex flex-col gap-3">
        <Link to={path.ZING_CHART} className="flex gap-2 items-center">
          <h3 className="text-3xl text-white font-bold hover:text-[#13A6A6]">
            #zingchart
          </h3>
          <span className="p-1 rounded-full bg-white">
            <BsFillPlayFill size={18} color="#13A6A6" />
          </span>
        </Link>
        <div className="flex gap-4 text-white h-full">
          <div className="flex-4 flex flex-col gap-3">
            {items?.slice(0, 3)?.map((item, index) => (
              <SongItem
                key={item?.encodeId}
                item={item}
                order={index + 1}
                percent={Math.round((item.score * 100) / chart?.totalScore)}
              />
            ))}
            <div className="flex items-center justify-center">
              <button
                className={`px-7 border h-8 rounded-l-full rounded-r-full bg-[hsla(0,0%,100%,.07)] hover:bg-[#954ea7]`}
                onClick={() => navigate(path.ZING_CHART)}
              >
                Xem thêm
              </button>
            </div>
          </div>
          <div className="flex-7 h-full relative">
            {data?.labels?.length > 0 && (
              <Line data={data} ref={chartRef} options={options} />
            )}
            <div
              className="tooltip rounded-md w-[320px]"
              style={{
                top: tooltip.top,
                left: tooltip.left,
                opacity: tooltip.opacity,
                position: "absolute",
                backgroundColor: "#c0d8d8",
              }}
            >
              {selected && (
                <SongItem item={items?.find((i) => i.encodeId === selected)} />
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

// eslint-disable-next-line react-refresh/only-export-components
export default memo(RankTopChart);
