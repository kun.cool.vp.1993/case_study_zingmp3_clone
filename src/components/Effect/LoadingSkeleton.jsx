import { ThreeCircles } from "react-loader-spinner";

function LoadingSkeleton() {
  return (
    <ThreeCircles
      visible={true}
      height="100"
      width="100"
      color="#13A6A6"
      ariaLabel="three-circles-loading"
      wrapperStyle={{}}
      wrapperClass=""
    />
  );
}

export default LoadingSkeleton;
