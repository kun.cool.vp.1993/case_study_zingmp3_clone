import { memo } from "react"
import PropTypes from "prop-types"
import { RotatingLines } from "react-loader-spinner"

// eslint-disable-next-line react-refresh/only-export-components
function PendingEffect({size}) {
  return (
  <RotatingLines
    visible={true}
    height={size}
    width={size}
    strokeColor="#000"
    strokeWidth="5"
    animationDuration="0.75"
    ariaLabel="rotating-lines-loading"
    wrapperStyle={{}}
    wrapperClass="pending-effect"
    />)
}

PendingEffect.propTypes = { 
  size: PropTypes.number
}
// eslint-disable-next-line react-refresh/only-export-components
export default memo(PendingEffect)