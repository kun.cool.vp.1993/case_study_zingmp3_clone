/* eslint-disable react-refresh/only-export-components */
import { memo } from "react";
import { Audio } from "react-loader-spinner";

function PlayingEffect() {
  return (
    <Audio
      height="30"
      width="30"
      color="white"
      ariaLabel="audio-loading"
      wrapperStyle={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        marginTop: "5px"
      }}
      wrapperClass="playing-effect"
      visible={true}
    />
  );
}

export default memo(PlayingEffect);
