import PropTypes from "prop-types";
import { useRef, useState } from "react";
import { icons } from "../../utilities";
import { useNavigate } from "react-router-dom";

const { BsThreeDots, AiOutlineHeart, BsFillPlayFill } = icons;

function Image({ src, isRoundedFull, hideIcon, link }) {
  const [isHover, setIsHover] = useState(false);
  const navigate = useNavigate();
  const imgRef = useRef();

  const handleHover = () => {
    setIsHover(true);
    imgRef.current.classList?.add("animate-scale-up-image");
    imgRef.current.classList?.remove("animate-scale-down-image");
  };
  const handleLeave = () => {
    setIsHover(false);
    imgRef.current.classList?.remove("animate-scale-up-image");
    imgRef.current.classList?.add("animate-scale-down-image");
  };
  return (
    <div
      onMouseEnter={handleHover}
      onMouseLeave={handleLeave}
      className={`cursor-pointer w-full relative overflow-hidden ${
        isRoundedFull ? "rounded-full" : "rounded-md"
      } `}
    >
      {isHover && (
        <div onClick={()=> navigate(link)}
         className="absolute top-0 left-0 right-0 bottom-0 rounded-md bg-overlay-hover z-10 flex items-center justify-evenly text-white">
          {!hideIcon && (
            <>
              <span>
                <AiOutlineHeart size={25} />
              </span>
              <span className="border border-white rounded-full ">
                <BsFillPlayFill size={45} />
              </span>
              <span>
                <BsThreeDots size={25} />
              </span>
            </>
          )}
        </div>
      )}
      <img
        ref={imgRef}
        src={src}
        alt="thumbnail"
        className="h-auto w-auto rounded-md "
        onClick={()=> navigate(link)}
      />
    </div>
  );
}

Image.propTypes = {
  src: PropTypes.string,
  isRoundedFull: PropTypes.bool,
  hideIcon: PropTypes.bool,
  link: PropTypes.string,
};

export default Image;
