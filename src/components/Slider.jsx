import { memo, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getArrSlider } from "../utilities/helper";
import { setCurrSongId } from "../store/actions";
import { useNavigate } from "react-router-dom";
import * as actions from "../store/actions";

// eslint-disable-next-line react-refresh/only-export-components
function Slider() {
  const listBanners = useSelector((state) => state?.app?.banner?.items) || [];
  // console.log(listBanners);
  const dispatch = useDispatch();
  const navigate = useNavigate();
  useEffect(() => {
    const sliderElements = document.getElementsByClassName("slider-item");
    let min = 0;
    let max = 2;
    let size = sliderElements.length;
    if (size > 0) {
      const intervalId = setInterval(() => {
        const list = getArrSlider(min, max, size - 1);
        for (let i = 0; i < size; i++) {
          sliderElements[i]?.classList?.remove(
            "animate-slide-right",
            "animate-slide-left",
            "order-last",
            "order-2",
            "order-first",
            "z-20",
            "z-1"
          );
          sliderElements[i].style.display = list.some((item) => item === i)
            ? "block"
            : "none";
        }
        list.forEach((item) => {
          if (item === max) {
            sliderElements[item]?.classList?.add(
              "animate-slide-right",
              "order-last",
              "z-20"
            );
          } else if (item === min) {
            sliderElements[item]?.classList?.add(
              "animate-slide-first",
              "order-first",
              "z-10"
            );
          } else {
            sliderElements[item]?.classList?.add(
              "animate-slide-left",
              "order-2",
              "z-10"
            );
          }
        });
        min = min === size - 1 ? 0 : min + 1;
        max = max === size - 1 ? 0 : max + 1;
      }, 2000);
      return () => intervalId && clearInterval(intervalId);
    }
  }, [listBanners.length]);
  const handleClickBanner = (item) => {
    if (item?.type === 1) {
      dispatch(setCurrSongId(item.encodeId));
      dispatch(actions.atAlbum([]));
    } else if (item?.type === 4) {
      const albumPath = item?.link?.split(".")[0];
      navigate(albumPath);
    } else {
      dispatch(actions.atAlbum([]));
    }
  };

  if (listBanners.length > 0)
    return (
      <div className="flex gap-4 mx-[59px] overflow-hidden pt-8">
        {listBanners?.map((item, i) => (
          <img
            key={item.encodeId}
            src={item.banner}
            onClick={() => handleClickBanner(item)}
            className={` cursor-pointer flex-1 object-contain w-1/4 rounded-lg slider-item ${
              i >= 3 ? "hidden" : ""
            }`}
          />
        ))}
      </div>
    );
}

// eslint-disable-next-line react-refresh/only-export-components
export default memo(Slider);
