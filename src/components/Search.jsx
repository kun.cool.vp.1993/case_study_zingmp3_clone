import { useRef, useState } from "react";
import { icons, path } from "../utilities";
import { useDispatch } from "react-redux";
import * as actions from "../store/actions";
import { useNavigate, createSearchParams } from "react-router-dom";

const { FiSearch, GrClose } = icons;

function Search() {
  const inputRef = useRef();
  const navigate = useNavigate();
  const [keyword, setKeyword] = useState("");
  const dispatch = useDispatch();
  const handleSearch = async (e) => {
    if (e.keyCode === 13) {
      console.log("enter");
      dispatch(actions.commonSearch(keyword));
      navigate({ 
        pathname: `${path.SEARCH}/${path.ALL}`,
        search: createSearchParams({ 
          q: keyword
        }).toString()
      });
    }
  };
  return (
    <div className="w-full flex items-center  text-gray-500 relative">
      {keyword && <span onClick={()=>{
        setKeyword('')
        inputRef.current.focus();
        }} className="absolute right-[16px] cursor-pointer opacity-50"><GrClose /></span>}
      <span className="h-10 px-4 flex items-center bg-[#dde4e4] justify-center rounded-s-[20px] cursor-pointer hover:text-gray-400">
        <FiSearch size={24} />
      </span>
      <input
        type="text"
        value={keyword}
        onChange={(e) => setKeyword(e.target.value)}
        onKeyUp={handleSearch}
        ref={inputRef}
        className="outline-none  pe-4 py-2 bg-[#dde4e4] h-10 w-full rounded-e-[20px]"
        placeholder="Tìm kiếm bài hát, nghệ sĩ, lời bài hát..."
      />
    </div>
  );
}

export default Search;
