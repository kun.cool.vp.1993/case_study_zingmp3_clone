import icons from "../../utilities/icon";
import * as actions from "../../store/actions";
import { convertSecondsToMins, shuffleArray } from "../../utilities/helper";
import PendingEffect from "../Effect/PendingEffect";
import { useDispatch, useSelector } from "react-redux";
import { useEffect, useRef, useState } from "react";
import { repeatMode } from "../../store/reducer/musicReducer";
const {
  MdSkipPrevious,
  MdSkipNext,
  CiRepeat,
  CiShuffle,
  BsFillPlayFill,
  BsPauseFill,
} = icons;

function PlayMusicSection() {
  const dispatch = useDispatch();
  const progressBarRef = useRef();
  const trackRef = useRef();
  const intervalRef = useRef();
  const atAlbumRef = useRef();

  const {
    currSongId,
    isPlaying,
    isPending,
    isShuffle,
    repeat,
    songInfo,
    audio,
    nextSongId,
    prevSongId,
    atAlbum,
    volume
  } = useSelector((state) => state.music);
  const [currentSeconds, setCurrentSeconds] = useState("00:00");
  // console.log(audio);
  useEffect(() => {
    dispatch(actions.fetchSongInfoAsync(currSongId));
    dispatch(actions.fetchAudioAsync(currSongId));
    dispatch(actions.playFile(true))
  }, [currSongId,dispatch])
  

  useEffect(() => {
    if (isShuffle && atAlbum.length > 1) {
      atAlbumRef.current = atAlbum;
      dispatch(actions.atAlbum(shuffleArray(atAlbum)));
    }
    if (!isShuffle) {
      dispatch(actions.atAlbum(atAlbumRef.current));
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isShuffle]);

  useEffect(() => {
    if (Array.isArray(atAlbum)) {
      const nextIndex = atAlbum.indexOf(currSongId) + 1;
      const prevIndex = atAlbum.indexOf(currSongId) - 1;
      if (prevIndex < 0) {
        dispatch(actions.setPrevSongId(null));
      } else {
        dispatch(actions.setPrevSongId(atAlbum[prevIndex]));
      }
      if (nextIndex >= atAlbum.length) {
        if (repeat === repeatMode.NONE) {
          dispatch(actions.setNextSongId(null));
        } else if (repeat === repeatMode.ALL) {
          // console.log(atAlbum[0]);
          dispatch(actions.setNextSongId(atAlbum[0]));
        }
      } else {
        dispatch(actions.setNextSongId(atAlbum[nextIndex]));
      }
    }
  }, [currSongId, repeat, atAlbum, dispatch]);

  useEffect(() => {
    
    intervalRef.current && clearInterval(intervalRef.current);
    intervalRef.current = undefined;
    audio.load();
    audio.currentTime = 0;
    if (!intervalRef.current) {
      intervalRef.current = setInterval(() => {
        let percentProgress =
          Math.round((audio.currentTime * 10000) / songInfo?.duration) / 100;
        progressBarRef.current.style.cssText = `right: ${100 - percentProgress}%`;
        setCurrentSeconds(
          convertSecondsToMins(audio.currentTime).replace("00:", "")
        );
      }, 200);
    }
    return () => {
      // audio.pause();
      audio.currentTime = 0;
      audio.src = "";
    };
  }, [audio, songInfo]);

  useEffect(() => {
    if (isPlaying) {
      if(audio.src == undefined || audio.src === ''){
        dispatch(actions.setPending(true));
        dispatch(actions.playFile(false))
        dispatch(actions.fetchAudioAsync(currSongId)); 
        audio.pause();
      }else{

        audio.play();
        audio.onended = () => {
          // console.log('ended');
          if (repeat === repeatMode.ONE) {
            audio.currentTime = 0;
            setCurrentSeconds("00:00");
            audio.play();
          }
          if (nextSongId) {
            dispatch(actions.setCurrSongId(nextSongId));
          }
        };
       }
    } else {
      audio.pause();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isPlaying, audio, dispatch]);

 
  

  const handleClickTogglePlayMusic = () => {
    if (!isPending) {
      if (isPlaying) {
        dispatch(actions.playFile(false));
      } else if(audio.src && audio.src !==''){ 
          dispatch(actions.playFile(true));
      }else dispatch(actions.setCurrSongId(currSongId))
      
    }
  };

  const handleForwardClick = (e) => {
    const trackRect = trackRef.current.getBoundingClientRect();
    const percentProgress =
      Math.round(((e.clientX - trackRect.left) * 10000) / trackRect.width) /
      100;
    progressBarRef.current.cssText = `right: ${100 - percentProgress}%`;
    audio.currentTime = (songInfo?.duration * percentProgress) / 100;
    setCurrentSeconds(Math.round((percentProgress * songInfo?.duration) / 100));
  };

  const handleNextSong = () => {
    // console.log('next song');
    if (nextSongId) {
      // console.log('next song');
      dispatch(actions.playFile(false));
      dispatch(actions.setCurrSongId(nextSongId));
    }
  };

  const handlePrevSong = () => {
    
    if (prevSongId) {
      
      dispatch(actions.setPending(true));
      dispatch(actions.setCurrSongId(prevSongId));
    }
  };

  const handleShuffle = () => {
    dispatch(actions.setShuffle(!isShuffle));
  };

  const handleRepeatClick = () => {
    switch (repeat) {
      case repeatMode.NONE:
        dispatch(actions.setRepeatMode(repeatMode.ALL));
        break;
      case repeatMode.ALL:
        dispatch(actions.setRepeatMode(repeatMode.ONE));
        break;
      case repeatMode.ONE:
        dispatch(actions.setRepeatMode(repeatMode.NONE));
        break;
      default:
        break;
    }
  };
  useEffect(() => {
    audio.volume = volume/100;
  }, [volume,audio])
  return (
    <div className="w-[40%] flex-auto flex items-center gap-2 justify-center flex-col py-2">
      <div className="flex gap-8 justify-center items-center">
        <span
          className={`cursor-pointer ${isShuffle && "text-purple-600"}`}
          title="Bật phát ngẫu nhiên"
          onClick={handleShuffle}
        >
          <CiShuffle size={24} />
        </span>
        <span
          onClick={handlePrevSong}
          // eslint-disable-next-line no-extra-boolean-cast
          className={`${!!prevSongId ? "cursor-pointer" : "text-gray-500 opacity-30"}`}
        >
          <MdSkipPrevious size={24} title={prevSongId} />
        </span>
        <span
          onClick={handleClickTogglePlayMusic}
          className={`p-1 border rounded-full border-gray-700 hover:text-main-500 
          ${!isPending && "cursor-pointer"} h-[50px] w-[50px] flex items-center justify-center`}
        >
          {isPending ? (
            <PendingEffect size={30} />
          ) : isPlaying ? (
            <BsPauseFill size={40} />
          ) : (
            <BsFillPlayFill size={40} />
          )}
        </span>
        <span
          onClick={handleNextSong}
          // eslint-disable-next-line no-extra-boolean-cast
          className={`${!!nextSongId ? "cursor-pointer" : "text-gray-500 opacity-30"}`}
        >
          <MdSkipNext size={24} title={nextSongId} />
        </span>
        <span
          className={`cursor-pointer ${repeat !== repeatMode.NONE && "text-purple-600"}`}
          onClick={handleRepeatClick}
        >
          {repeat === repeatMode.ONE ? (
            <div className="relative flex">
              <div className="absolute top-[5px] left-[9px] text-[10px] z-first font-bold">
                1
              </div>
              <CiRepeat size={24} title="Tắt phát lại" className="z-last" />
            </div>
          ) : (
            <CiRepeat
              size={24}
              title={`${repeat === repeatMode.NONE ? "Bật phát lại tất cả" : "Bật phát lại một bài"}`}
            />
          )}
        </span>
      </div>
      <div className="w-full flex items-center justify-center gap-3 text-xs">
        <span>{currentSeconds}</span>
        <div
          className="relative h-[3px] hover:h-[8px] w-3/4 bg-[rgba(0,0,0,0.1)] rounded-l-full rounded-r-full cursor-pointer"
          id="progress-bar"
          ref={trackRef}
          onClick={handleForwardClick}
        >
          <div
            className="absolute top-0 left-0 bottom-0 bg-main-500 rounded-l-full rounded-r-full cursor-pointer"
            id="current-progress"
            ref={progressBarRef}
          ></div>
        </div>
        <span>
          {songInfo?.duration
            ? convertSecondsToMins(songInfo?.duration).replace("00:", "")
            : "00:00"}
        </span>
      </div>
    </div>
  );
}



export default PlayMusicSection;
