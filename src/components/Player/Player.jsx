
import SongInfoSection from "./SongInfoSection";
import PlayMusicSection from "./PlayMusicSection";
import VolumeSection from "./VolumeSection";



function Player() {
  return (
    <div className="bg-main-400 px-5 h-full flex">
      <SongInfoSection/>
      <PlayMusicSection />
      <VolumeSection />
    </div>
  );
}

export default Player;