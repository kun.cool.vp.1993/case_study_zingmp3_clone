import { useSelector } from "react-redux";
import icons from "../../utilities/icon";
const {
  // AiFillHeart,
  AiOutlineHeart,
  BsThreeDots,
} = icons;

function SongInfoSection() {
    const {songInfo} = useSelector(st=>st.music)

  return (
    <div className="w-[30%] flex-auto flex items-center gap-3">
      <img
        src={songInfo?.thumbnail}
        alt="thumbnail"
        className="w-16 h-16 object-cover rounded-md"
      />
      <div className="flex flex-col">
        <span className="font-semibold text-gray-700 text-sm">
          {songInfo?.title || "Ngày Trái Tim Rơi Lệ"}
        </span>
        <span className="text-xs text-grey-500">
          {songInfo?.artistsNames || "Nguyễn Đình Vũ"}
        </span>
      </div>
      <div className="flex gap-4 pl-2">
        <span className="cursor-pointer">
          <AiOutlineHeart size={16} />
        </span>
        <span className="cursor-pointer">
          <BsThreeDots size={16} />
        </span>
      </div>
    </div>
  );
}

export default SongInfoSection;
