import { useRef} from "react";
import { useDispatch, useSelector } from "react-redux";

import icons from "../../utilities/icon";
import * as actions from "../../store/actions";
import { getIconVolume } from "../../utilities/helper";

const { BsMusicNoteList } = icons;

function VolumeSection() {
  const {volume} = useSelector(st => st.music)
  const dispatch = useDispatch();
  const volumeRef = useRef();
  const { showSidebar } = useSelector((state) => state.music);
  const handleShowSidebar = () => {
    dispatch(actions.setShowSidebar(!showSidebar));
  };

  const handleChangeVolume = (e) => {
    const volumeValue = Number(e.target.value);
    if (volumeValue === 0) {
      volumeRef.current = 5;
    }
    volumeRef.current = volumeValue;
    dispatch(actions.setVolume(volumeValue));
  };
  const VolumeIcon = getIconVolume(volume / 100);
  const handleMuteVolume = () =>{
    let expectedVolume;
    if (volume > 0) {
      volumeRef.current = Number(volume);
      expectedVolume =  0;
    } else {
      expectedVolume = volumeRef.current;
    }
    dispatch(actions.setVolume(expectedVolume));
  }

  return (
      <div className="w-[30%] flex-auto  flex items-center justify-end gap-3">
      <span className="cursor-pointer"
            onClick={handleMuteVolume}>
        <VolumeIcon size={20} />
      </span>
        <input
            type="range"
            step={1}
            max={100}
            min={0}
            value={volume}
            onInput={(e) => handleChangeVolume(e)}
            style={{
              width: "70px",
              height: "5px",
              background: "white",
              outline: "none",
            }}
        />
        <div className="h-[28px] rounded-full w-[1px] bg-gray-500 mx-[10px] opacity-20"></div>
        <span
            className={`
          flex items-center justify-center
            h-[30px]
            w-[30px]
            cursor-pointer 
            p-1 
            rounded-md
            ${showSidebar && "bg-main-500"}
           hover:bg-main-500 
           opacity-90 
           hover:opacity-100`}
            onClick={handleShowSidebar}
        >
        <BsMusicNoteList size={16} />
      </span>
      </div>
  );
}
export default VolumeSection;
