// import PropTypes from "prop-types";
import TrackMusic from "./TrackMusic";
import { memo } from "react";
import { icons } from "../utilities";
import { useSelector } from "react-redux";
const { BsArrowDownUp } = icons;

// eslint-disable-next-line react-refresh/only-export-components
function Lists() {
  const songItems = useSelector((st) => st.album.album.song.items);
  // console.log(songItems);
  return (
    <div className="w-full flex flex-col text-xs  text-gray-500 ">
      <div className="flex justify-between items-center p-[10px] font-semibold">
        <span className="flex gap-2 flex-10">
          <span className="">
            <BsArrowDownUp />
          </span>
          <span className="">BÀI HÁT</span>
        </span>
        <span className="flex-8 text-left">ALBUM</span>
        <span className="flex-2 text-right">THỜI GIAN</span>
      </div>

      <div className="flex flex-col">
        {songItems.map((item) => {
          return (
            <TrackMusic
              key={item.encodeId}
              songData={item}
              playlist = {songItems}
            />
          );
        })}
      </div>
    </div>
  );
}

// Lists.propTypes = {
//   items: PropTypes.array,
// }
//eslint-disable-next-line react-refresh/only-export-components
export default memo(Lists);
