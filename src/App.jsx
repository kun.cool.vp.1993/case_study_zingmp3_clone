import { Route, Routes } from "react-router-dom";
import "./App.css";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import {
  Album,
  Home,
  Login,
  NewRelease,
  Personal,
  Public,
  Singer,
  WeekChart,
  ZingChart,
} from "./containers/public";
import path from "./utilities/path";
import { useEffect } from "react";
import * as actions from "./store/actions";
import { useDispatch } from "react-redux";
import "react-loading-skeleton/dist/skeleton.css";
import { AllCategories, ArtistCategory, PlaylistCategory, Search, TitleCategory, VideoCategory } from "./containers/public";

function App() {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(actions.getHome());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  return (
    <>
      <div className="">
        <Routes>
        <Route path={path.LOGIN} element={<Login />} />
          <Route path={path.PUBLIC} element={<Public />}>
            <Route path={path.HOME} element={<Home />} />
            <Route path={path.NEW_RELEASE} element={<NewRelease />} />
            <Route path={path.PERSONAL} element={<Personal />} />
            <Route path={path.ALBUM__TITLE__PID} element={<Album />} />
            <Route path={path.PLAYLIST__TITLE__PID} element={<Album />} />
            <Route path={path.WEEKCHART__TITLE__PID} element={<WeekChart />} />
            <Route path={path.ZING_CHART} element={<ZingChart />} />
            <Route path={path.HOME__SINGER} element={<Singer />} />
            <Route path={path.SEARCH} element={<Search />}>
              <Route path={path.ALL} element={<AllCategories />} />
              <Route path={path.SINGER} element={<ArtistCategory />} />
              <Route path={path.PLAYLIST} element={<PlaylistCategory />} />
              <Route path={path.SONG} element={<TitleCategory />} />
              <Route path={path.MV} element={<VideoCategory />} />
            </Route>
            <Route path={path.DEFAULT} element={<Home />} />
          </Route>
        </Routes>
      </div>
      <ToastContainer
        position="top-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme="light"
      />

      <ToastContainer />
    </>
  );
}

export default App;
