const path = {
  PUBLIC: "/",
  HOME: "",

  LOGIN: "login",

  PERSONAL: "personal",
  
  NEW_RELEASE: "new-release/:title",
  ALBUM__TITLE__PID: "album/:title/:pid",
  PLAYLIST__TITLE__PID: "playlist/:title/:pid",
  WEEKCHART__TITLE__PID: "zing-chart-tuan/:title/:pid",
  ZING_CHART: "zing-chart",

  HOME__SINGER: ':singer',

  SEARCH: "tim-kiem",
  SONG: 'bai-hat',
  ALL: 'tat-ca',
  SINGER: 'artist',
  PLAYLIST: 'playlist',
  MV: 'video',

  DEFAULT: "*",
};

export default path;
