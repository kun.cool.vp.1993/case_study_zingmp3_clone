import moment from "moment";
import icons from "./icon";
// import * as actions from "../store/actions";

export function getArrSlider(start, end, number) {
  const limit = start > end ? number : end;
  let output = [];
  for (let i = start; i <= limit; i++) {
    output.push(i);
  }
  if (start > end) {
    for (let i = 0; i <= end; i++) {
      output.push(i);
    }
  }
  return output;
}

export function getDisplayStr(titleStr, expectedSize) {
  if (Number.isInteger(expectedSize) && expectedSize > 0) {
    if (titleStr.length > expectedSize) {
      const lastSpaceIndex = titleStr.lastIndexOf(" ", expectedSize - 1);
      const truncatedStr =
        lastSpaceIndex !== -1
          ? titleStr.slice(0, lastSpaceIndex)
          : titleStr.slice(0, expectedSize);
      return truncatedStr + "...";
    } else {
      return titleStr;
    }
  }
  return "unknown";
}

export function convertSecondsToMins(seconds) {
  return moment.utc(seconds * 1000).format("HH:mm:ss");
}

export function convertToCamelCase(inputString) {
  return inputString.replace(/-([a-z])/g, (match, group) =>
    group.toUpperCase()
  );
}
export function checkReleaseDate(releaseTimestamp) {
  const releaseDate = new Date(releaseTimestamp * 1000);
  const currentDate = new Date();
  currentDate.setHours(0, 0, 0, 0);
  if (releaseDate.toDateString() === currentDate.toDateString()) {
    return "Hôm nay";
  } else {
    const timeDiff = currentDate.getTime() - releaseDate.getTime();
    const daysDiff = Math.floor(timeDiff / (1000 * 60 * 60 * 24));
    if (daysDiff === 1) return "Hôm qua";
    if (daysDiff < 7) return `${daysDiff} ngày trước`;
    return `${Math.floor(daysDiff / 7)} tuần trước`;
  }
}
export const getIconVolume = (volume) => {
  if (volume === 0) return icons.IoVolumeMuteOutline;
  else if (volume > 0 && volume <= 0.33) return icons.IoVolumeLowOutline;
  else if (volume > 0.33 && volume <= 0.67) return icons.IoVolumeMediumOutline;
  return icons.IoVolumeHighOutline;
};

export const convertArrayToPlaylistObj = (playlists) => {
  if (Array.isArray(playlists))
    return playlists.reduce((acc, curr) => {
      const { sectionId, ...rest } = curr;
      return {
        ...acc,
        [sectionId]: rest,
      };
    }, {});
};

export function extractIdFromPath(path) {
  const regex = /\/([^/]+)\.html$/;
  const match = path.match(regex);
  return match ? match[1] : null;
}

export function shuffleArray(array) {
  const shuffledArray = [...array];

  for (let i = shuffledArray.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [shuffledArray[i], shuffledArray[j]] = [shuffledArray[j], shuffledArray[i]];
  }

  return shuffledArray;
}

export function getRecentQueueList(array, addItem) {
  if (Array.isArray(array) && typeof addItem !== 'undefined') {
    const newArray = [...array.filter(i=>i.encodeId !== addItem.encodeId)].slice(0,19);
    return [addItem, ...newArray]; 
  }
  return [];
}

export const handleNumber = number => { 
  if(number>Math.pow(10,6)){ 
    return `${Math.round(number*10/ Math.pow(10,6))/10}M`;
  }else { 
    return`${Math.round(number*10/ Math.pow(10,3))/10}K`;
  }
}