import {
  MdOutlineLibraryMusic,
  MdSkipPrevious,
  MdSkipNext,
} from "react-icons/md";
import { BiDisc } from "react-icons/bi";
import { IoAnalyticsOutline } from "react-icons/io5";
import { CgNotes } from "react-icons/cg";
import {
  AiOutlineArrowLeft,
  AiOutlineArrowRight,
  AiOutlineHeart,
  AiFillHeart,
} from "react-icons/ai";
import { HiArrowNarrowLeft, HiArrowNarrowRight } from "react-icons/hi";
import { FiSearch,FiLogIn } from "react-icons/fi";
import {
  BsThreeDots,
  BsFillPlayFill,
  BsPauseFill,
  BsArrowDownUp,
  BsDot,
  BsMusicNoteList,
} from "react-icons/bs";
import { CiRepeat, CiShuffle, CiMusicNote1 } from "react-icons/ci";
import { FaChevronRight, FaPlay } from "react-icons/fa";
import {
  IoVolumeHighOutline,
  IoVolumeMediumOutline,
  IoVolumeLowOutline,
  IoVolumeMuteOutline,
  IoVolumeOffOutline,
} from "react-icons/io5";
import { IoMdAlarm } from "react-icons/io";
import { AiOutlineUserAdd } from "react-icons/ai";

import { ImBin } from "react-icons/im";
import { GrClose } from "react-icons/gr";

const icons = {
  MdOutlineLibraryMusic,
  BiDisc,
  IoAnalyticsOutline,
  CgNotes,
  AiOutlineArrowRight,
  AiOutlineArrowLeft,
  HiArrowNarrowLeft,
  HiArrowNarrowRight,
  FiSearch,
  AiOutlineHeart,
  AiFillHeart,
  BsThreeDots,
  CiRepeat,
  MdSkipPrevious,
  MdSkipNext,
  CiShuffle,
  BsFillPlayFill,
  BsPauseFill,
  CiMusicNote1,
  BsArrowDownUp,
  BsDot,
  FaChevronRight,
  FaPlay,
  BsMusicNoteList,
  IoVolumeHighOutline,
  IoVolumeMediumOutline,
  IoVolumeLowOutline,
  IoVolumeMuteOutline,
  IoVolumeOffOutline,
  ImBin,
  IoMdAlarm,
  AiOutlineUserAdd,
  GrClose,
  FiLogIn
};
export default icons;
