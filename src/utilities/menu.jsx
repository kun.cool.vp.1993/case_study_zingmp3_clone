import icons from "./icon"
import path from "./path"

const {MdOutlineLibraryMusic,BiDisc,IoAnalyticsOutline,CgNotes} = icons

export const sidebarMenuItems =[
    {
        path: 'personal',
        text: 'Cá nhân',
        icon: <MdOutlineLibraryMusic size={24} />
    },
    {
        path: 'discovery',
        text: 'Khám phá',
        end: true,
        icon: <BiDisc size={24}/>
    },
    {
        path: path.ZING_CHART,
        text: '#zingchart',
        icon: <IoAnalyticsOutline size={24} />
    },
    {
        path: 'follow',
        text: 'Theo dõi',
        icon: <CgNotes size={24} />
    },
]
export const searchMenuItems =[
    {
        path: path.ALL,
        text: 'TẤT CẢ',

    },
    {
        path: path.SONG,
        text: 'BÀI HÁT',
        // end: true,

    },
    {
        path: path.SINGER,
        text: 'NGHỆ SĨ/OA',

    },
    {
        path: path.PLAYLIST,
        text: 'PLAYLIST/ALBUM',

    },
    {
        path: path.MV,
        text: 'MV',

    },
]// export default sidebarMenuItems