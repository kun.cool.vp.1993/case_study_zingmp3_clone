
import { NavLink, Outlet } from "react-router-dom";
import { searchMenuItems } from "../../../utilities";
import { useSelector } from "react-redux";

const notActiveStyle = 'px-4 hover:text-main-500 font-semibold cursor-pointer';
const activeStyle = 'px-4 hover:text-main-500 font-semibold cursor-pointer text-main-500 h-[52px] flex items-center rounded-lg'

function Search() {

  const keyword =useSelector(st=>st.search.keyword);

  return (
    <div>
      <div className="flex pl-[60px] h-[50px] mb-7 items-center text-sm border-b border-gray-400 pb-1 pt-2">
        <span className="text-[24px] font-bold pr-6 border-r border-gray-400">Kết quả tìm kiếm</span>
        <div className="flex items-center justify-center">
      {searchMenuItems.map(item => (
        <NavLink key={item.path}
        to={`${item.path}?q=${keyword.replaceAll(' ','+')}`}
        className={({isActive})=>isActive?activeStyle:notActiveStyle} >
          {item.text}
        </NavLink>
      ))}


        </div>
      </div>
      <div>
        <Outlet />
      </div>
    </div>
  );
}

export default Search;
