import { useSelector } from "react-redux";
import { handleNumber, path } from "../../../utilities";
import { SongItem } from "../../../components/NewRelease";
import TrackMusic from "../../../components/TrackMusic";
import Playlist from "../../../components/Playlist";
import Artist from "../../../components/Artist";

function AllCategories() {
  const { searchCommonData } = useSelector((st) => st.search);
  console.log(searchCommonData);
  return (
    <div className="w-full flex flex-col px-[60px]">
      <div>
        <h3 className="text-lg font-bold mb-4">Nổi bật</h3>
        <div className="flex gap-8">
          {searchCommonData?.top && (
            <div className="flex flex-1 gap-6 items-center h-[100px] bg-main-200 rounded-md">
              <img
                src={searchCommonData.top.thumbnail}
                alt="avatar"
                className={` mx-3 h-[84px] w-[84px] object-cover ${
                  searchCommonData.top.objectType === "artist"
                    ? "rounded-full"
                    : "rounded-md"
                }`}
              />
              <div className="flex flex-col text-xs">
                <span className="mb-[6px]">
                  {searchCommonData.top.objectType === "artist"
                    ? "Nghệ sĩ"
                    : ""}
                </span>
                <span className="text-sm font-semibold">
                  {searchCommonData.top.title || searchCommonData.top.name}
                </span>
                {searchCommonData.top.objectType === "artist" && (
                  <span className="">
                    {handleNumber(searchCommonData.artists[0]?.totalFollow) +
                      " quan tâm"}
                  </span>
                )}
              </div>
            </div>
          )}
          {searchCommonData?.songs
            ?.filter((item, index) =>
              [...Array(2).keys()].some((i) => i === index)
            )
            ?.map((item) => (
              <div key={item.encodeId} className="flex-1">
                <SongItem
                  key={item.encodeId}
                  item={item}
                  size="w-[84px] h-[84px]"
                  bgColor="bg-main-200"
                />
              </div>
            ))}
        </div>
      </div>
      <div className="flex flex-col w-full pt-8">
        <h3 className="text-lg font-bold mb-5">Bài hát</h3>
        <div className="flex justify-between w-full flex-wrap">
          {searchCommonData.songs &&
            searchCommonData.songs.map((item, index) => (
              <div
                key={item.encodeId}
                className={`flex-auto w-[45%] ${
                  index % 2 !== 0 ? "pl-4" : "pr-4"
                } ${index >= 8 ? "hidden" : ""}`}
              >
                <TrackMusic
                  songData={item}
                  playlist={searchCommonData?.songs}
                  hideAlbum
                />
              </div>
            ))}
        </div>
      </div>
      <div className=" w-full">
        <Playlist
          playlist={{ items: searchCommonData?.playlists, title: "Album Hot" , link: path.PROFILE}}
          nonPadding
        >
          {searchCommonData?.top && (
            <div className="flex flex-start w-[300px] gap-2 items-center h-[100px]rounded-md">
              <img
                src={searchCommonData.top.thumbnail}
                alt="avatar"
                className={` mx-3 h-[70px] w-[70px] object-cover rounded-md
                `}
              />
              <div className="flex flex-col text-lg ">
                <span className="mb-[6px] opacity-70">PLAYLIST NỔI BẬT
                </span>
                <span className="text-sm font-semibold">
                  {searchCommonData.top.title || searchCommonData.top.name}
                </span>
              </div>
            </div>
          )}
        </Playlist>
        <div className="flex flex-col w-full mt-10">
          <h3 className="text-lg font-bold mb-5">Nghệ sĩ/OA</h3>
          <div className="flex gap-[28px]">
            {searchCommonData?.artists?.filter((i, index)=> index<=4).map((item)=>(
              <Artist 
               key={item.id}
               title={item.name}
               image={item.thumbnailM}
               followers={item.totalFollow}
               link={item.link}
              />
            ))}
          </div>
        </div>
      </div>
      <div className="h-[500px]"></div>
    </div>
  )
}

export default AllCategories;
// {...playlist, title: 'Playlist/Album'}
