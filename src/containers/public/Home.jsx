import { useSelector } from "react-redux";
import {
  LoadingSkeleton,
  NewRelease,
  Playlist,
  RankTopChart,
  Slider,
  WeekChart
} from "../../components";
import { convertArrayToPlaylistObj } from "../../utilities";
// import 'react-loading-skeleton/dist/skeleton.css'

function Home() {
  const { playlists, isLogged} = useSelector((state) => state.app);
  console.log(isLogged);
  // const {isLoadingAlbum} = useSelector((state) => state.album);
  // console.log(isLoadingAlbum);
  if (playlists === undefined || playlists === null) {
    return <LoadingSkeleton />;
  }
  const playlistObj = convertArrayToPlaylistObj(playlists);

  // console.log(playlistObj);
  return (
    <div className="w-full overflow-x-hidden">
      <Slider />
      <NewRelease />
      {playlistObj?.hEditorTheme && <Playlist key={playlistObj?.hEditorTheme?.title} playlist={playlistObj?.hEditorTheme} />}
      {playlistObj?.hEditorTheme3 && <Playlist key={playlistObj?.hEditorTheme3?.title} playlist={playlistObj?.hEditorTheme3} />}
      {playlistObj?.hEditorTheme4 && <Playlist key={playlistObj?.hEditorTheme4?.title} playlist={playlistObj?.hEditorTheme4} />}
      <RankTopChart />
      <WeekChart />
      {playlistObj?.h100 && <Playlist key={playlistObj?.h100?.title} playlist={playlistObj?.h100} />}
      {playlistObj?.hAlbum && <Playlist key={playlistObj?.hAlbum?.title} playlist={playlistObj?.hAlbum} />}
      <div className="w-full h-[500px]"></div>
    </div>
  );
}

export default Home;
