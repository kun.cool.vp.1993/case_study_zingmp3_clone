export { default as Home } from "./Home";
export { default as Login } from "./Login";
export { default as Public } from "./Public";
export { default as Personal } from "./Personal";
export { default as Album } from "./Album";
export { default as NewRelease } from "./NewRelease";
export { default as WeekChart } from "./WeekChart";
export { default as ZingChart } from "./ZingChart";
export * from "./Search";

export { default as Singer} from './Singer';
