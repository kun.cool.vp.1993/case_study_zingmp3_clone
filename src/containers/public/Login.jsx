import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import * as actions from '../../store/actions'
import { useNavigate } from 'react-router-dom';
import { useDispatch } from 'react-redux';

const schema = yup.object().shape({
  email: yup.string().email('Email không hợp lệ').required('Email là bắt buộc'),
  password: yup.string().required('Mật khẩu là bắt buộc'),
});

const Login = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const { register, handleSubmit, formState: { errors } } = useForm({
    resolver: yupResolver(schema),
  });

  const onSubmit = (data) => {
    const {email, password} = data;
    if(email === 'admin@gmail.com' && password === 'admin') { 
      console.log('loggin');
      dispatch(actions.LogIn(true))
      navigate('/')
    }
  };

  return (
    <div className="min-h-screen flex items-center justify-center bg-main-400">
      <form onSubmit={handleSubmit(onSubmit)} className="bg-white p-8 rounded rounded-lg shadow-md">
        <h2 className="text-2xl font-semibold mb-4">Đăng nhập</h2>

        <div className="mb-4">
          <label htmlFor="email" className="block text-gray-600">Email:</label>
          <input type="text" id="email" {...register('email')} className={`mt-1 p-2 w-full border rounded-md ${errors.email ? 'border-red-500' : 'border-gray-300'}`} />
          {errors.email && <p className="text-red-500 text-sm">{errors.email.message}</p>}
        </div>

        <div className="mb-4">
          <label htmlFor="password" className="block text-gray-600">Mật khẩu:</label>
          <input type="password" id="password" {...register('password')} className={`mt-1 p-2 w-full rounded-md border ${errors.password ? 'border-red-500' : 'border-gray-300'}`} />
          {errors.password && <p className="text-red-500 text-sm">{errors.password.message}</p>}
        </div>

        <button type="submit" className="bg-blue-500 text-white px-4 py-2 rounded hover:bg-blue-600">Đăng nhập</button>
      </form>
    </div>
  );
};

export default Login;
