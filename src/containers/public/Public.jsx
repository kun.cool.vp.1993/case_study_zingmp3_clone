import { Outlet } from "react-router-dom";
import {
  Header,
  LoadingSkeleton,
  Player,
  SidebarLeft,

} from "../../components";
import { useSelector } from "react-redux";
import Scrollbars from "react-custom-scrollbars-2";
import { SidebarRight } from "../../components";

function Public() {
  const { showSidebar } = useSelector((st) => st.music);
  const { isLoadingAlbum } = useSelector(st=>st.album)
  const { isLoadingHome } = useSelector((st) => st.app);

  return (
    <div className="w-full relative h-screen flex flex-col bg-main-300 overflow-x-hidden">
      {isLoadingHome && (
        <div className="absolute top-0 bottom-0 z-50 left-0 right-0 bg-main-200 flex items-center justify-center">
          <LoadingSkeleton />
        </div>
      )}
      <div className="w-full h-full flex flex-auto overflow-y-auto ">
        <div className="w-[200px] h-full flex-none ">
          <SidebarLeft />
        </div>
        <div className="flex-auto relative flex flex-col">
          {isLoadingAlbum && (
            <div className="absolute top-0 bottom-0 z-20 left-0 right-0 bg-main-200 flex items-center justify-center">
              <LoadingSkeleton />
            </div>
          )}
          <div className="h-[70px] px-[59px] flex-none flex items-center mb-5">
            <Header />
          </div>
          <div className="flex-auto w-full">
            <Scrollbars autoHide style={{ width: "100%", height: "100%" }}>
              <Outlet />
            </Scrollbars>
          </div>
        </div>
        {/* z-50 absolute top-0 bottom-[90px] right-0 */}
        {
          <div
            className={`w-[329px] z-50 absolute top-0 bottom-[90px] h-display right-0 bg-[#FFF] ${
              showSidebar ? "animate-slide-left" : "animate-slide-hidden"
            }  `}
          >
            <SidebarRight />
          </div>
        }
      </div>
      <div className="flex-none h-[90px] z-40 fixed bottom-0 left-0 right-0">
        <Player />
      </div>
    </div>
  );
}

export default Public;
