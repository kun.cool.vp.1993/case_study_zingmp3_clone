import { useEffect } from "react";
import { useParams } from "react-router-dom";
import moment from "moment";
import { Lists, PlayingEffect } from "../../components";
import Scrollbars from "react-custom-scrollbars-2";
import { icons } from "../../utilities";
import { convertSecondsToMins } from "../../utilities/helper";
import { useDispatch, useSelector } from "react-redux";
import * as actions from "../../store/actions";

const { BsDot } = icons;

function Album() {
  const { pid } = useParams();
  // eslint-disable-next-line no-unused-vars
  const { currSongId, isPlaying, songs, isLoadingAlbum } = useSelector(
    (state) => state.music
  );
  const { album } = useSelector((st) => st.album);

  const dispatch = useDispatch();

  useEffect(() => {
    // dispatch(actions.setCurrPlaylist(pid))
    dispatch(actions.getAlbum(pid));
  }, [pid, dispatch]);
  return (
    <div className="flex gap-8 w-full px-[59px] h-screen">
      <div className="flex-none w-1/4 flex flex-col items-center gap-2">
        <div className="">
          <div className="w-full relative overflow-hidden">
            <img
              src={album?.thumbnailM}
              alt="thumbnail"
              className={`w-full object-contain ${
                isPlaying
                  ? "rounded-full animate-rotate-center"
                  : "rounded-md animate-rotate-pause "
              } shadow-5`}
            />
            <div
              className={`absolute top-0 right-0 bottom-0 left-0 cursor-pointer hover:bg-overlay-hover flex items-center justify-center ${
                isPlaying && "rounded-full"
              }`}
            >
              {isPlaying && (
                <div className="w-12 h-12 border border-white rounded-full">
                  <PlayingEffect />
                </div>
              )}
            </div>
          </div>
          <div className="flex flex-col items-center">
            <h3 className="text-[20px] font-bold text-gray-800 mt-3">
              {album?.title}
            </h3>
            <span className="flex gap-2 items-center justify-center text-gray-500 text-sm">
              <span>Updated: </span>
              <span>
                {moment.unix(album?.contentLastUpdate).format("DD/MM/YYYY")}
              </span>
            </span>
            <span className="text-center flex gap-2 items-center justify-center text-gray-500 text-sm">
              {album?.artistsNames}
            </span>
            <span className="flex gap-2 items-center justify-center text-gray-500 text-sm">
              {`${Math.round(album?.like / 1000)} K người yêu thích`}
            </span>
          </div>
        </div>
      </div>
      <div className="flex-auto">
        <span className="text-sm">
          <span className="text-gray-600">Lời tựa </span>
          <span className="font-semibold">{album?.sortDescription}</span>
        </span>
        <Scrollbars autoHide style={{ width: "100%", height: "80%" }}>
          <div>{album.song && <Lists />}</div>
        </Scrollbars>
        <div className="flex gap-1 text-gray-800 text-sm py-[10px]">
          <span className="pl-2">{`${album?.song?.total} bài hát`}</span>
          <BsDot size={24} />
          <span className="">{`${convertSecondsToMins(
            album?.song?.totalDuration
          )}`}</span>
        </div>
      </div>
    </div>
  );
}

export default Album;
