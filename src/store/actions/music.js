import actionTypes from "./actionTypes";
import * as apis from "../../apis";
import { toast } from "react-toastify";

export const setCurrSongId = (payload) => ({
  type: actionTypes.SET_CURRENT_SONG_ID,
  payload,
});
export const setNextSongId = (payload) => {
  return ({
  type: actionTypes.SET_NEXT_SONG_ID,
  payload,
});}

export const setPrevSongId = (payload) => ({
  type: actionTypes.SET_PREV_SONG_ID,
  payload,
})
export const playFile = (payload) => ({
  type: actionTypes.PLAY,
  payload,
});
export const atAlbum = (payload) => ({
  type: actionTypes.AT_ALBUM,
  payload,
});

export const setShowSidebar = (payload) => ({
  type: actionTypes.SHOW_SIDEBAR,
  payload,
});

export const setShuffle = (payload) => ({
  type: actionTypes.SET_SHUFFLE,
  payload,
});

export const setRepeatMode = (payload) => ({
  type: actionTypes.SET_REPEAT_MODE,
  payload,
})
export const setAudio = (payload) => ({ 
  type: actionTypes.SET_AUDIO,
  payload,
})

export const setVolume = (payload) => ({ 
  type: actionTypes.SET_VOLUME,
  payload,
})

export const setSongInfo = (payload) => ({ 
  type: actionTypes.SET_SONG_INFO,
  payload,
})
export const setPending = (payload) => ({ 
  type: actionTypes.SET_PENDING,
  payload,
})
export const setAutoPlay = (payload) => ({ 
  type: actionTypes.SET_AUTO_PLAY,
  payload,
})

export const setAutoNext = (payload) => ({ 
  type: actionTypes.SET_AUTO_NEXT,
  payload,
})
export const setCurrPlaylist = (payload) => ({
  type: actionTypes.SET_CURR_PLAYLIST,
  payload,
})

export const addToRecent  = (payload) => ({
  type: actionTypes.ADD_TO_RECENT_LIST,
  payload
})

export const fetchDetailPlaylist = (pid) => async (dispatch) => {
  try {
    const response = await apis.apiGetDetailPlaylist(pid);

    if (response.status === 200 && response?.data.err === 0) {
      dispatch({
        type: actionTypes.PLAYLIST,
        payload: response?.data?.data,
      });
    }
  } catch (error) {
    dispatch({
      type: actionTypes.PLAYLIST,
      payload: null,
    });
  }
};

export const fetchAudioAsync = (currSongId) => async (dispatch, getState) => { 
  dispatch(setPending(true));
  try {
    const response = await apis.apiGetSong(currSongId);
    if(response.status === 200 && response?.data.err === 0) { 
      if(response.data.data['128']){
        dispatch(setAudio(new Audio(response.data.data['128'])));
        dispatch(setPending(false));
        // dispatch(playFile(true));
      }
      else {
        toast.error('Not supported');
        dispatch(setPending(false));
        dispatch(playFile(false));
       }
    } else { 
      dispatch(setPending(false))
      dispatch(playFile(false))
      dispatch(setAudio(new Audio()));
      toast.warn(response.data?.msg)
      console.log(getState());
      dispatch(setCurrSongId(getState().music.nextSongId))
    }
  } catch (error) {
    dispatch(setPending(false));
    dispatch(playFile(false));
    dispatch(setAudio(new Audio()));
    toast.error(error.message);
    dispatch(setCurrSongId(getState().music.nextSongId))
  }
}

export const fetchSongInfoAsync = (currSongId) => async(dispatch) => { 
  dispatch(setPending(true));
  try {
    const response = await apis.apiGetDetailSong(currSongId);
    if (response.data.err === 0) {
          dispatch(setSongInfo(response.data.data))
          dispatch(setPending(false));
        }
  } catch (error) {
    dispatch(setPending(false));
    dispatch(playFile(false));
    toast.error(error.message);
  }
}
