import actionTypes from "./actionTypes";
import * as apis from "../../apis";
import { toast } from "react-toastify";

export const commonSearch = (keyword) => async (dispatch) =>{ 
    try {
      const response = await apis.apiSearch(keyword);
      if(response?.data.err === 0) { 
        dispatch(setSearchCommon(response?.data?.data))
        dispatch(setSearchKeyword(keyword))
      }else { 
        dispatch(setSearchCommon({}))
        dispatch(setSearchKeyword(''))
        toast.warn(response?.data?.data?.msg)
      }
    } catch (error) {
      dispatch(setSearchCommon({}));
      dispatch(setSearchKeyword(''))
      toast.error(error.message);
    }
}

const setSearchCommon = payload => ({
  type: actionTypes.SEARCH_COMMON,
  payload
})

const setSearchKeyword = payload => ({
  type: actionTypes.SEARCH_KEYWORD,
  payload
})

