import * as apis from "../../apis";
import actionTypes from "./actionTypes";


export const getHome = () => async (dispatch) => {
    try {
        dispatch(loadingHome(true))
        const response = await apis.getHome();
        dispatch(loadingHome(false))
        if(response.status === 200) {
            const items = response.data.data.items; 
            dispatch({ 
                type: actionTypes.GET_HOME,
                payload: items
            });
        }else { 
            //handle failure
            dispatch({ 
                type: actionTypes.GET_HOME,
                payload: null,
            })
        }
    } catch (e) {
        dispatch({ 
            type: actionTypes.GET_HOME,
            payload: null,
        })
    }
};
export const loadingHome = (payload) => ({
    type: actionTypes.LOADING_HOME,
    payload,
});

export const LogIn = (payload) => ({ 
    type: actionTypes.LOGIN,
    payload
})
