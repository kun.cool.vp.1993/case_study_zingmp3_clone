import * as apis from "../../apis";
import actionTypes from "./actionTypes";


export const loadingAlbum = (payload) => ({
  type: actionTypes.LOADING_ALBUM,
  payload,
});

export const getAlbum = (albumId) => async (dispatch) => {
    try {
        dispatch(loadingAlbum(true))
        const response = await apis.apiGetDetailPlaylist(albumId);
        dispatch(loadingAlbum(false))
        if(response.status === 200 && response?.data.err === 0) {
            const album = response.data.data; 
            dispatch(setAlbum(album));
        }else { 
            dispatch({ 
                type: actionTypes.GET_HOME,
                payload: null,
            })
        }
    } catch (e) {
        dispatch({ 
            type: actionTypes.GET_HOME,
            payload: null,
        })
    }
};

const setAlbum = (payload) => ({ 
    type: actionTypes.SET_ALBUM,
    payload
} ) 
