import { applyMiddleware, legacy_createStore as createStore } from "redux";
import rootReducer from "./reducer/rootReducer";
import { thunk } from "redux-thunk";
import persistStore from "redux-persist/es/persistStore";

const reduxConfig = () => {
  const store = createStore(rootReducer, applyMiddleware(thunk));
  const persistor = persistStore(store);
  return {
    store,
    persistor,
  };
};
export default reduxConfig;
