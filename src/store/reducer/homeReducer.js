import { convertToCamelCase } from "../../utilities/helper";
import actionTypes from "../actions/actionTypes";

const initialState = {
  isLoadingHome: false,
  isLogged: false
};
const homeReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.GET_HOME:
      console.log(action.payload);
      if (Array.isArray(action.payload)) {
        let part1 = { playlists: [] };
        const part2 = action.payload.reduce(
          (acc, curr) => {
            const { sectionType, ...rest } = curr;
            if (sectionType !== "playlist") {
              acc[convertToCamelCase(sectionType)] = rest;
            } else {
              part1 = { playlists: [...part1.playlists, rest] };
            }
            return acc;
          },
          { ...state }
        );
        return { ...part2, ...part1 };
      }
      return state;
    case actionTypes.LOADING_HOME:
      return {
        ...state,
        isLoadingHome: action.payload,
      };
      case actionTypes.LOGIN:
      return {
        ...state,
        isLogged: action.payload,
      };
    default:
      return state;
  }
};

export default homeReducer;
