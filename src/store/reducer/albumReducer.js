import actionTypes from "../actions/actionTypes";

const initialState = { 
    isLoadingAlbum: false,
    album: {},
} 


const albumReducer = (state = initialState, action) => {
    switch (action.type) {
      case actionTypes.SET_ALBUM:
        return {
          ...state,
          album: action.payload || null,
        };
        case actionTypes.LOADING_ALBUM:
        return {
          ...state,
          isLoadingAlbum: action.payload,
        };
      default:
        return state;
    }
  };
  
  export default albumReducer;