import { getRecentQueueList } from "../../utilities";
import actionTypes from "../actions/actionTypes";

export const repeatMode = {
  NONE: "NONE",
  ONE: "REPEAT_ONE",
  ALL: "REPEAT_ALL",
};
const initialState = {
  currSongId: null,
  nextSongId: null,
  prevSongId: null,
  playlist: null,
  isPlaying: false,
  atAlbum: [],
  showSidebar: false,
  audio: new Audio(),
  volume: 50,
  isShuffle: false,
  isPending: false,
  repeat: repeatMode.NONE,
  songInfo: null,
  isAutoNext: true,
  isAutoPlay: true,
  recentList: [],
};

const musicReducer = (state = initialState, action) => {
  switch (action.type) {


    case actionTypes.SET_CURR_PLAYLIST:
      return {
        ...state,
        playlist: action.payload,
      };
    case actionTypes.SET_AUTO_NEXT:
    return {
      ...state,
      isAutoNext: action.payload,
    };
    case actionTypes.SET_AUTO_PLAY:
      return {
        ...state,
        isAutoPlay: action.payload,
      };
    case actionTypes.SET_CURRENT_SONG_ID:
      return {
        ...state,
        currSongId: action.payload,
      };
    case actionTypes.SET_NEXT_SONG_ID:
      return {
        ...state,
        nextSongId: action.payload,
      };
    case actionTypes.SET_PREV_SONG_ID:
      return {
        ...state,
        prevSongId: action.payload,
      };
    case actionTypes.SET_SONG_INFO:
      return {
        ...state,
        songInfo: action.payload,
      };
    case actionTypes.PLAY:
      return {
        ...state,
        isPlaying: action.payload,
      };
    case actionTypes.AT_ALBUM:
      return {
        ...state,
        atAlbum: action.payload || [],
      };
    case actionTypes.SHOW_SIDEBAR:
      return {
        ...state,
        showSidebar: action.payload,
      };
    case actionTypes.SET_AUDIO:
      return {
        ...state,
        audio: action.payload,
      };
    case actionTypes.SET_SHUFFLE:
      return {
        ...state,
        isShuffle: action.payload,
      };
    case actionTypes.SET_REPEAT_MODE:
      return {
        ...state,
        repeat: action.payload,
      };
    case actionTypes.SET_PENDING:
      return {
        ...state,
        isPending: action.payload,
      };
      case actionTypes.SET_VOLUME:
      return {
        ...state,
        volume: action.payload,
      };
      case actionTypes.ADD_TO_RECENT_LIST:
        console.log(state.recentList);
        return {
          ...state,
          recentList: getRecentQueueList(state.recentList, action.payload),
        };
    default:
      return state;
  }
};

export default musicReducer;
