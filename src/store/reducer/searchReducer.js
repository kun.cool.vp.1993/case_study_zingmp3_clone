
import actionTypes from "../actions/actionTypes";

const initialState = {
  searchCommonData: {},
  keyword: '',
};
const searchReducer = (state = initialState, action) => {
  switch (action.type) {
    
    case actionTypes.SEARCH_COMMON:
      return {
        ...state,
        searchCommonData: action.payload || {},
      };
      case actionTypes.SEARCH_KEYWORD:
        return {
          ...state,
          keyword: action.payload || '',
        };
    default:
      return state;
  }
};

export default searchReducer;
