import { combineReducers } from "redux";
import homeReducer from "./homeReducer";
import storage from "redux-persist/lib/storage";
import autoMergeLevel2 from "redux-persist/es/stateReconciler/autoMergeLevel2";
import musicReducer from "./musicReducer";
import persistReducer from "redux-persist/es/persistReducer";
import albumReducer from "./albumReducer";
import searchReducer from "./searchReducer";

const commonConfig = {
  storage: storage,
  stateReconciler: autoMergeLevel2,
};

const musicConfig = {
  ...commonConfig,
  key: "music",
  whitelist: [
    "currSongId",
    "nextSongId",
    "prevSongId",
    "isPlaying",
    "atAlbum",
    "showSidebar",
    "isShuffle",
    "isPending",
    "repeat",
    "songInfo",
    "playlist",
    "recentList"
  ],
};

const rootReducer = combineReducers({
  app: homeReducer,
  music: persistReducer(musicConfig, musicReducer),
  album: albumReducer,
  search: searchReducer
});

export default rootReducer;
