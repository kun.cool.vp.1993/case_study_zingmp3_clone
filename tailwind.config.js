/** @type {import('tailwindcss').Config} */
export default {
  content: ["./index.html", "./src/**/*.{js,ts,jsx,tsx}"],
  theme: {
    extend: {
      backgroundColor: {
        "main-100": "#e7ecec",
        "main-200": "#dde4e4",
        "main-300": "#ced9d9",
        "main-400": "#c0d8d8",
        "main-500": "#13A6A6",
        "main-600": '#0e8080',
        'overlay-hover': "rgba(0,0,0,0.3)",
      },
      colors: {
        "main-100": "#e7ecec",
        "main-200": "#dde4e4",
        "main-300": "#ced9d9",
        "main-400": "#c0d8d8",
        "main-500": "#0e8080",
      },
      backgroundImage: {
        'image-hover': "radial-gradient(circle, #ffffff, #b9b9b9, #777777, #3b3b3b, #000000);"
      },
      keyframes: {
        "slide-right": {
          "0%": {
            "-webkit-transform": "translateX(-700px);",
            transform: "translateX(-700px);",
          },
          "100%": {
            "-webkit-transform": "translateX(0);",
            transform: "translateX(0);",
          },
        },
        "slide-hidden": {
          "0%": {
            "-webkit-transform": "translateX(0px);",
            transform: "translateX(0px);",
          },
          "100%": {
            "-webkit-transform": "translateX(400px);",
            transform: "translateX(400px);",
          },
        },
        "slide-left": {
          "0%": {
            "-webkit-transform": "translateX(300px);",
            transform: "translateX(300px);",
          },
          "100%": {
            "-webkit-transform": "translateX(0);",
            transform: "translateX(0);",
          },
        },
        "slide-first": {
          "0%": {
            "-webkit-transform": "translateX(310px);",
            transform: "translateX(310px);",
          },
          "100%": {
            "-webkit-transform": "translateX(0);",
            transform: "translateX(0);",
          },
        },
        "rotate-center": {
          "0%": {
            "-webkit-transform": "rotate(0);",
            transform: "rotate(0);",
          },
          "100%": {
            "-webkit-transform": "rotate(360deg);",
            transform: "rotate(360deg);",
          },
        },

        "rotate-pause": {
          "0%": {
            "-webkit-transform": "rotate(360deg);",
            transform: "rotate(360deg);",
            "border-radius": "99999px",
          },
          "100%": {
            "-webkit-transform": "rotate(0);",
            transform: "rotate(0);",
          },
        },
        "scale-up-center":{ 
          "0%": {
            "-webkit-transform": "scale(0.5);",
            transform: "scale(0.5);",
          },
          "100%": {
            "-webkit-transform": "scale(1);",
            transform: "scale(1);",
          },
        },
        "scale-up-image":{ 
          "0%": {
            "-webkit-transform": "scale(1);",
            transform: "scale(1);",
          },
          "100%": {
            "-webkit-transform": "scale(1.2);",
            transform: "scale(1.2);",
          },
        },
        "scale-down-image":{ 
          "0%": {
            "-webkit-transform": "scale(1.2);",
            transform: "scale(1.2);",
          },
          "100%": {
            "-webkit-transform": "scale(1);",
            transform: "scale(1);",
          },
        },
        
      },
      animation: {
        "slide-right":
          "slide-right 1s cubic-bezier(0.250, 0.460, 0.450, 0.940) both;",
        "slide-left":
          "slide-left 1s cubic-bezier(0.250, 0.460, 0.450, 0.940) both;",
          "slide-hidden":
          "slide-hidden 1s cubic-bezier(0.250, 0.460, 0.450, 0.940) both;",
        "slide-first":
          "slide-first 1s cubic-bezier(0.250, 0.460, 0.450, 0.940) both;",
        "rotate-center": "rotate-center 8s linear infinite;",
        "rotate-pause": "rotate-pause 0.3s linear 1 both;",
        "scale-up-center": "scale-up-center 0.4s cubic-bezier(0.390, 0.575, 0.565, 1.000) both;",
        "scale-up-image": "scale-up-image 0.4s cubic-bezier(0.390, 0.575, 0.565, 1.000) both;",
        "scale-down-image": "scale-down-image 0.4s cubic-bezier(0.390, 0.575, 0.565, 1.000) both;"
      },
      flex: {
        4: "4 4 0%",
        2: "2 2 0%",
        3: "3 3 0%",
        5: "5 5 0%",
        6: "6 6 0%",
        7: "7 7 0%",
        8: "8 8 0%",
        10: "10 10 0%",
      },
      boxShadow: {
        5: "rgba(0, 0, 0, 0.24) 0px 3px 8px;",
      },
      height: {
        display: "calc(100vh - 90px)",
      },
    },
    screens: {
      1600: "1600px",
    },
  },
  plugins: [],
};
